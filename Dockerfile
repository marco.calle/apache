FROM ubuntu:bionic
LABEL maintainer="marco.calle@istea.com.ar"
LABEL version="1.1"
LABEL description="TP 2 - Actualización Tecnologica - apache"
# El proyecto esta basado en honeypothttpd https://github.com/bocajspear1/honeyhttpd.git
# Se customiza para mostrar una página de login de Herbalife
# El honeypot captura usuario, clave y dirección IP del posible atacante
# El contenedor tendra 3 puertos abiertos
# honeypot: 80 - 443
# sitio para ver el log: 5443

ARG DEBIAN_FRONTEND=noninteractive
#Instalacion de stack lamp, python y pip
#RUN apt update && apt install -y python python-pip
#Instalaición de dependencia de honeyhttpd
#RUN pip install termcolor

#Instalacion de stack lamp
RUN apt update && apt install -y vim apache2 php libapache2-mod-php php-mysql php-curl php-zip && apt autoremove && apt clean

ENV APPSERVERNAME herbalife.com
ENV APPALIAS www.herbalife.com
ENV APPSERVERADMIN marco.calle@istea.com.ar
#ENV MYSQL_USER honeypot
#ENV MYSQL_USER_PASSWORD q1w2e3r4
#ENV MYSQL_DB_NAME honeypot

#Creación de directorio /app/
RUN mkdir -p /app

#Copia de archivos de configuracion para cambiar el puerto por defecto de apache
COPY default.conf /app
COPY ports.conf /app
COPY index.php /app


#Directorio para cambiar hacer cambios sobre el archivo ApacheServer.py
#RUN mkdir -p /app/update

#Carpeta update para cambiar la configuracion de ApacheServer.py
#Se define volumen html para cambiar el codigo del viso del log
#VOLUME ["/app/update","/var/www/html"]

#Copia de Honeypot
#COPY honey.tgz /app/


#Entrypoint para inicializar Apache y honeypot
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
