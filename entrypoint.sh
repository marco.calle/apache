#!/bin/bash
set -e

#Copia de paquete honeyhttpd
# cd /app && tar -zxvf honey.tgz
# chmod 777 -R /app/honeyhttpd/

#Creacion de archivos de log del honeypot
#[[ -f /app/honeyhttpd/logs/server-http-80.log ]]&& echo ' ' > /app/honeyhttpd/logs/server-http-80.log || touch /app/honeyhttpd/logs/server-http-80.log
#[[ -f /app/honeyhttpd/logs/server-https-443.log ]]&& echo ' ' > /app/honeyhttpd/logs/server-https-443.log || touch /app/honeyhttpd/logs/server-https-443.log


#Visor del log
#Se elimina el directorio /var/www/html
if [ -f /var/www/html/index.html ]; then
	rm -r /var/www/html/*
fi

# Modificar los valores del fichero default.conf 'ServerAlias' y 'ServerName'
[ -f /etc/apache2/sites-available/000-default.conf ] && rm /etc/apache2/sites-available/000-default.conf

cp /app/default.conf /etc/apache2/sites-available/default.conf
a2ensite default.conf
cp /app/ports.conf /etc/apache2/
cp /app/index.php /var/www/html/

sed -i 's/APPSERVERADMIN/'"$APPSERVERADMIN"'/' /etc/apache2/sites-available/default.conf
sed -i 's/APPSERVERNAME/'"$APPSERVERNAME"'/' /etc/apache2/sites-available/default.conf
sed -i 's/APPALIAS/'"$APPALIAS"'/' /etc/apache2/sites-available/default.conf
echo 'ServerName herbalife.com' >> /etc/apache2/apache2.conf

apachectl -D FOREGROUND
#cd /app/honeyhttpd/ &&  python start.py --config config.json

exec "$@"
